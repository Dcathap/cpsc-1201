/*----------------------------------
Dylan Cathapermal
Username: dcathap
Lab: Lab5
Section 003
TA: Nushrat Humaira
-----------------------------------*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  //made the deck so that each set of 4 would have every suit 
  //(2heart, 2spade, 2heart, 2clubs, 3heart etc...)
  for(int j=0;j<52;j+=4){
    deck[j].suit =SPADES;
    deck[j].value=(j/4)+2;
    deck[j+1].suit=HEARTS;
    deck[j+1].value=(j/4)+2;
    deck[j+2].suit=DIAMONDS;
    deck[j+2].value=(j/4)+2;
    deck[j+3].suit=CLUBS;
    deck[j+3].value=(j/4)+2;
  }
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(std::begin(deck),std::end(deck),myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card arr[5] = {deck[0], deck[1], deck[2], deck[3],deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
  sort(arr,arr+5,suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for(int c=0;c<5;c++){
    cout << setw(5) << get_card_name(arr[c]) << " of " << get_suit_code(arr[c]) << "\n";
  }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  //IMPLEMENT
  //check if the left suit is lower value than the right suit
  if(lhs.suit<rhs.suit){
    return true;
  }
  //the oppositve case than the above case
  else if(lhs.suit>rhs.suit){
    return false;
  }
  //this is the case where the suits are the same and then I check value
  if(lhs.suit==rhs.suit){
    if(lhs.value<rhs.value){
      return true;
    }
    else
      return false;
  }
  return true;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  //switch case for wahtever value the card has (mostly important for the face cards)
  switch(c.value){
    case 2: return "2";
    case 3: return "3";
    case 4: return "4";
    case 5: return "5";
    case 6: return "6";
    case 7: return "7";
    case 8: return "8";
    case 9: return "9";
    case 10: return "10";
    case 11: return "Jack";
    case 12: return "Queen";
    case 13: return "King";
    case 14: return "Ace";
    default: return "";
  }
  // IMPLEMENT
}
